@extends('layouts.app')

@section('content')
<div class="container" id="app">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Weekly Average</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <example-component></example-component>
<hr>
                    <form method="post">
                      @csrf
                      <h2>Add New Game</h2>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Date</label>
                        <input type="date" class="form-control" name="date" placeholder="">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Location</label>
                        <input type="text" class="form-control" name="location" placeholder="ie. Hamilton Bowl">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Score</label>
                        <input type="text" class="form-control" name="score" placeholder="Be honest now...">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Main Ball</label>
                        <input type="text" class="form-control" name="main_ball" placeholder="ie. Storm Reign of Fire">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Secondary Ball</label>
                        <input type="text" class="form-control" name="alt_ball" placeholder="ie. Columbia 300 Yellow Dot">
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="is_practice" value="1" checked>
                        <label class="form-check-label" for="is_practice">
                          Practice Game
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="is_league" value="1">
                        <label class="form-check-label" for="is_league">
                          League Game
                        </label>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">League Name</label>
                        <input type="text" class="form-control" name="league" placeholder="Only enter if this was a league game.">
                      </div>
                      <br>
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
