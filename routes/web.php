<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/data', 'GameController@all')->name('data');
Route::get('/avg', 'GameController@avg')->name('avg');


Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home', 'GameController@addGame')->name('addGame');
