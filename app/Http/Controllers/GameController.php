<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Game;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class GameController extends Controller
{
    public function all()
    {
      return Game::all();
    }

    public function avg()
    {
      $avg = DB::select('select week(date) as Week ,round(avg(score)) as Average from games where user_id =' . Auth::id() . ' group by week(date)');
      return $avg;
    }

    public function addGame(Request $request)
    {
      $newGame              = new Game;
      $newGame->user_id     = Auth::id();
      $newGame->date        = $request->date;
      $newGame->main_ball   = $request->main_ball;
      $newGame->alt_ball    = $request->alt_ball;
      $newGame->location    = $request->location;
      $newGame->score       = $request->score;
      $newGame->is_practice = $request->is_practice;
      $newGame->is_league   = $request->is_league;
      $newGame->league      = $request->league;
      $newGame->save();

      return redirect('home');

    }
}
